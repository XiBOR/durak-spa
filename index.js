const http = require('http');
const fs = require('fs');
const port = process.env.PORT || 5000;
const build = '/dist/build.js';
const app = http.createServer((req,res) => {
  res.writeHead(200);
  res.end(fs.readFileSync(__dirname + (req.url == build ? build : '/index.html')));
});

app.listen(port, () => console.log('https://durak-spa.herokuapp.com:' + port));
