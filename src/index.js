import Vue from 'vue'
import App from './components/app.vue'

var vm = new Vue({
  el: '.application',
  render: h => h(App)
})
